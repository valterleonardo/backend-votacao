# Bem vindo a Aplicação BackEnd do Sistema de Votações

## Tecnologias
- SpringBoot
- Lombook
- JPA
- Swagger2
- Maven
- H2
- Cacheable
- Autenticação JWT

#### Iniciando a Aplicação - Spring local
- Crie uma pasta para o projeto
- Baixe o projeto ou dê um `git clone`
- Dê um clean no projeto com o maven `mvn clean`
- Compile o projeto `mvn install`
- Inicialize o projeto - `mvn spring-boot:run`

#### Acessar documentação da aplicação com Swagger
- Após subir a aplicação, acessar: `http://localhost:8081/swagger-ui.html`


#### BANCO DE DADOS
- utilizar o profile `h2` para teste local em memória, H2 DataBase.
- utilizar o profile `mysql` para teste com banco persistente, Mysql DatBase (ajustar `application-mysql.yml` para conexao correta).
- em IDE, colocar o VM args: `--spring.profiles.active=XX` onde XX é o profile escolhido.


#### Realizar Testes
- Utilizar collection do postaman disponibilizada na raiz do projeto, seguir a ordem, atualizando o token
- usuarios: admin/admin e client/client
- `back-end-votacao.postman_collection.json`