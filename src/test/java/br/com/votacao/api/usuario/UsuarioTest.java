package br.com.votacao.api.usuario;

import br.com.votacao.api.usuario.entity.Usuario;
import br.com.votacao.api.usuario.service.UsuarioService;
import br.com.votacao.core.security.enums.Role;
import br.com.votacao.core.security.exception.SecurityException;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class UsuarioTest {

    @Autowired
    private UsuarioService usuarioService;

    private String token;

    @Test
    public void cadastrarUsuarioComSucesso(){
        token = usuarioService.signup(getUsuario());
        Assert.notNull(token);
    }

    @Test
    public void cadastrarUsuarioComErro(){
        Usuario usuario = getUsuario();
        usuario.setUsername(null);
        assertThrows(DataIntegrityViolationException.class, () -> {
            usuarioService.signup(usuario);
        });
    }

    @Test
    public void logarComSucesso(){
        String token = usuarioService.signin("admin", "admin");
        Assert.notNull(token);
    }

    @Test
    public void reautenticarUsuarioComSucesso(){
        token = usuarioService.refresh("admin");
        Assert.notNull(token);
    }

    @Test
    public void reautenticarUsuarioComErro(){
        assertThrows(SecurityException.class, () -> {
            usuarioService.refresh("sandro");
        });
    }

    private Usuario getUsuario() {
        Usuario usuario = new Usuario();
        usuario.setUsername("Valter");
        usuario.setPassword("123456");
        usuario.setEmail("valter@valter.com");
        usuario.setRoles(Arrays.asList(Role.ROLE_ADMIN, Role.ROLE_CLIENT));
        return usuario;
    }
}
