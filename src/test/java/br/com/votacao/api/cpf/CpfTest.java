package br.com.votacao.api.cpf;

import br.com.votacao.api.cpf.dto.CpfDTO;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class CpfTest {

    @Autowired
    private ConsultaCPFClient consultaCPFClient;

    @Test
    public void consultaCpfValidoRetornaObject(){
        CpfDTO cpfDTO = consultaCPFClient.buscaCpf("47703012009");
        Assert.notNull(cpfDTO);
    }

    @Test
    public void consultaCpfInvalidoRetornaErro(){
        Exception exception = assertThrows(HttpClientErrorException.class, () -> {
            consultaCPFClient.buscaCpf("17703012009");
        });
        assertTrue(exception.getMessage().contains("Not Found"));
    }
}
