package br.com.votacao.api.pauta;

import br.com.votacao.api.pauta.dto.PautaDTO;
import br.com.votacao.api.pauta.service.PautaService;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class PautaTest {

    @Autowired
    private PautaService pautaService;

    @Test
    public void cadastrarPautaComSucesso(){
        PautaDTO pautaDTO = getPauta();
        pautaDTO = pautaService.cadastrar(pautaDTO);
        Assert.notNull(pautaDTO.getId());
    }

    @Test
    public void cadastrarPautaComErro(){
        PautaDTO pautaDTO = new PautaDTO();
        assertThrows(DataIntegrityViolationException.class, () -> {
            pautaService.cadastrar(pautaDTO);
        });
    }

    @Test
    public void buscarListaDePautas(){
        pautaService.listar();
    }

    private PautaDTO getPauta() {
        PautaDTO pautaDTO = new PautaDTO();
        pautaDTO.setDescricao("pauta 1");
        return pautaDTO;
    }
}