package br.com.votacao.api.voto;

import br.com.votacao.api.cpf.ConsultaCPFClient;
import br.com.votacao.api.cpf.exception.CpfException;
import br.com.votacao.api.pauta.dto.PautaDTO;
import br.com.votacao.api.pauta.service.PautaService;
import br.com.votacao.api.sessao.dto.SessaoDTO;
import br.com.votacao.api.sessao.service.SessaoService;
import br.com.votacao.api.usuario.dto.UsuarioDTO;
import br.com.votacao.api.voto.dto.VotoDTO;
import br.com.votacao.api.voto.entity.Voto;
import br.com.votacao.api.voto.enums.Escolha;
import br.com.votacao.api.voto.service.VotoService;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class VotoTest {

    @Autowired
    private VotoService votoService;
    @Autowired
    private PautaService pautaService;
    @Autowired
    private SessaoService sessaoService;

    @MockBean
    private RestTemplate restTemplate;
    @MockBean
    private ConsultaCPFClient consultaCPFClient;

    private PautaDTO pautaDTO;
    private SessaoDTO sessaoDTO;
    private UsuarioDTO usuarioDTO;

    @BeforeEach
    public void init(){

        pautaDTO = new PautaDTO();
        pautaDTO.setDescricao("Pauta 1");
        pautaDTO = pautaService.cadastrar(pautaDTO);

        sessaoDTO = new SessaoDTO();
        sessaoDTO.setPautaDTO(pautaDTO);
        sessaoDTO.setPeriodo(60);
        sessaoDTO.setDescricao("Sessao 01");
        sessaoDTO = sessaoService.cadastrar(sessaoDTO);

        usuarioDTO = new UsuarioDTO();
        usuarioDTO.setId(1);
    }

    @Test
    @Order(1)
    public void votarEmSessaoComErroDeCPFInvalido(){
        VotoDTO voto = getVotoDTONao();
        voto.setCpf("47703012");
        Mockito.doThrow(new CpfException("", HttpStatus.BAD_REQUEST)).when(consultaCPFClient).validarCpf("47703012");
        assertThrows(CpfException.class, () -> {
            votoService.votar(voto);
        });
    }

    @Test
    @Order(2)
    public void votarEmSessaoNao(){
        Mockito.doNothing().when(consultaCPFClient).validarCpf("37063430025");
        VotoDTO votoDTO = votoService.votar(getVotoDTONao());
        Assert.notNull(votoDTO);
        System.out.println(votoDTO);
    }

    @Test
    @Order(3)
    public void votarEmSessaoSim() {
        Mockito.doNothing().when(consultaCPFClient).validarCpf("47703012009");
        VotoDTO votoDTO = votoService.votar(getVotoDTOSim());
        Assert.notNull(votoDTO);
        System.out.println(votoDTO);
    }

    @Test
    @Order(4)
    public void buscarVotosSessao(){
        Mockito.doNothing().when(consultaCPFClient).validarCpf("47703012009");
        votoService.votar(getVotoDTOSim());
        List<Voto> votos = votoService.buscarVotosPorSessaoId(sessaoDTO.getId());
        Assert.notEmpty(votos);
    }

    private VotoDTO getVotoDTONao() {
        VotoDTO votoDTONao = new VotoDTO();
        votoDTONao.setEscolha(Escolha.NAO);
        votoDTONao.setCpf("37063430025");
        votoDTONao.setData(new Date());
        votoDTONao.setUsuarioDTO(usuarioDTO);
        votoDTONao.setSessaoDTO(sessaoDTO);
        votoDTONao.getSessaoDTO().setPautaDTO(pautaDTO);
        return votoDTONao;
    }

    private VotoDTO getVotoDTOSim() {
        VotoDTO votoDTOSim = new VotoDTO();
        votoDTOSim.setEscolha(Escolha.SIM);
        votoDTOSim.setCpf("47703012009");
        votoDTOSim.setUsuarioDTO(usuarioDTO);
        votoDTOSim.setSessaoDTO(sessaoDTO);
        votoDTOSim.getSessaoDTO().setPautaDTO(pautaDTO);
        votoDTOSim.setData(new Date());
        return votoDTOSim;
    }
}