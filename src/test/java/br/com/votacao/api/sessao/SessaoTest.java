package br.com.votacao.api.sessao;

import br.com.votacao.api.pauta.dto.PautaDTO;
import br.com.votacao.api.pauta.service.PautaService;
import br.com.votacao.api.sessao.dto.SessaoDTO;
import br.com.votacao.api.sessao.dto.TotalSessaoDTO;
import br.com.votacao.api.sessao.entity.Sessao;
import br.com.votacao.api.sessao.service.SessaoService;
import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class SessaoTest {

    @Autowired
    private SessaoService sessaoService;
    @Autowired
    private PautaService pautaService;

    private PautaDTO pautaDTO;

    @BeforeEach
    public void init(){
        pautaDTO = pautaService.cadastrar(new PautaDTO("Pauta 1"));
    }

    @Test
    @Order(1)
    @Transactional
    public void cadastrarSessaoComSucesso(){
        SessaoDTO sessaoDTO = getSessao();
        sessaoDTO = sessaoService.cadastrar(sessaoDTO);
        Assert.notNull(sessaoDTO.getId());
    }

    @Test
    @Order(2)
    public void cadastrarSessaoComErro(){
        SessaoDTO sessaoDTO = getSessao();
        sessaoDTO.setDescricao(null);
        assertThrows(DataIntegrityViolationException.class, () -> {
            sessaoService.cadastrar(sessaoDTO);
        });
    }

    @Test
    @Order(3)
    public void listarSessoesCadastradas(){
        List<SessaoDTO> sessaoDTOList = sessaoService.listar();
        Assert.notNull(sessaoDTOList);
        Assert.notNull(sessaoDTOList.size());
        System.out.println(sessaoDTOList);
    }

    @Test
    @Order(4)
    public void buscarSessaoPorId(){
        sessaoService.cadastrar(getSessao());
        Sessao sessao = sessaoService.getSessao(1L);
        System.out.println(sessao.toString());
    }

    @Test
    @Order(5)
    public void totalizarSessaoPorId(){
        TotalSessaoDTO totalSessaoDTO = sessaoService.totalizar(1L);
        Assert.notNull(totalSessaoDTO);
    }

    private SessaoDTO getSessao() {
        SessaoDTO sessaoDTO = new SessaoDTO();
        sessaoDTO.setData(new Date());
        sessaoDTO.setDescricao("Sessao 1");
        sessaoDTO.setPeriodo(60);
        sessaoDTO.setPautaDTO(pautaDTO);

        return sessaoDTO;
    }
}