package br.com.votacao.api.sessao.exception;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class SessaoException extends RuntimeException implements Serializable {

    private final String message;
    private final HttpStatus httpStatus;

    public SessaoException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}