package br.com.votacao.api.cpf.exception;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class CpfException extends RuntimeException implements Serializable {

    private final String message;
    private final HttpStatus httpStatus;

    public CpfException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
