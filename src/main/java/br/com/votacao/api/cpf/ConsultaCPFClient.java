package br.com.votacao.api.cpf;

import br.com.votacao.api.cpf.dto.CpfDTO;
import br.com.votacao.api.cpf.enums.StatusCpf;
import br.com.votacao.api.cpf.exception.CpfException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
@RequiredArgsConstructor
public class ConsultaCPFClient {

    private final RestTemplate restTemplate = new RestTemplate();

    public CpfDTO buscaCpf(String cpf){
        CpfDTO cpfDTO = restTemplate.getForObject("https://user-info.herokuapp.com/users/{cpf}", CpfDTO.class, cpf);
        log.info("Consulta CPF: " + cpfDTO.toString());
        return cpfDTO;
    }

    public void validarCpf(String cpf) {
        if(cpf == null || cpf.length() < 11)
            throw new CpfException("CPF inválido", HttpStatus.BAD_REQUEST);

        CpfDTO cpfDTO = this.buscaCpf(cpf);

        if(cpfDTO.getStatus().equals(StatusCpf.UNABLE_TO_VOTE)){
            throw new CpfException("CPF não está apto a votar", HttpStatus.BAD_REQUEST);
        }
    }
}
